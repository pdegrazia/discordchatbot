URLS = {
    '/wiki': 'https://fbghostita.miraheze.org/',
    '/calibrazioni': 'https://fbghostita.miraheze.org/wiki/Calibrazioni',
    '/upgrades': 'https://fbghostita.miraheze.org/wiki/Upgrades',
    '/tt': 'https://fbghostita.miraheze.org/wiki/Slicer',
    '/tmc': 'https://fbghostita.miraheze.org/wiki/Upgrade_a_tmc2209',
}
