import os

import discord
from dotenv import load_dotenv

from src.urls import URLS

load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')
GUILD = os.getenv('DISCORD_GUILD')

client = discord.Client()

@client.event
async def on_ready():
    print('{} has connected to Discord!'.format(client.user))

@client.event
async def on_message(message):
    print('received a message')
    if message.author == client.user:
        return

    if str(message.channel) != 'comandibot':
        if message.content in URLS.keys():
            response = URLS[message.content]
            await message.channel.send(response)


client.run(TOKEN)
